package com.github.tjeukayim.chat.client.views

import com.github.tjeukayim.chat.Binary
import com.github.tjeukayim.chat.User
import com.github.tjeukayim.chat.client.Chat
import javafx.beans.property.SimpleStringProperty
import javafx.scene.Node
import tornadofx.*
import java.time.format.DateTimeFormatter

class MainView : View() {
    private val loginModel: LoginModel by inject()
    private val chat = Chat(loginModel.username.get())
    private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")

    override val root = vbox {
        listview(chat.messages) {
            cellFormat { message ->
                graphic = borderpane {
                    top = label(message.user.name)
                    center = formatMessage(message.content)
                    bottom = label(message.time.format(timeFormatter))
                }
            }
        }
        form {
            fieldset("New message") {
                val text = SimpleStringProperty()
                textarea(text)
                button("Send") {
                    action {
                        chat.sendText(text.value)
                    }
                    // Disable button is chat in not connected
                    disableProperty().bind(chat.connectedProperty.not())
                }
            }
        }
    }

    private fun formatMessage(content: Any): Node = when (content) {
    // Text-message
        is String -> textarea(content) { isEditable = false }
    // Status update
        is User.Status -> label(
            when (content) {
                User.Status.ONLINE -> "Joined the chat"
                User.Status.OFFLINE -> "Left the chat"
            }
        )
    // Binary
        is Binary -> label("Name: ${content.name}, Size: ${content.bytes.size}")
        else -> throw IllegalStateException("Invalid message content")
    }
}
