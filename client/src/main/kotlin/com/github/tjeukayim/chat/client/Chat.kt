package com.github.tjeukayim.chat.client

import com.github.tjeukayim.chat.Binary
import com.github.tjeukayim.chat.ChatClient
import com.github.tjeukayim.chat.ChatServer
import com.github.tjeukayim.chat.User
import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import tornadofx.getValue
import tornadofx.observable
import tornadofx.setValue
import java.time.LocalTime

class Chat(username: String) {
    val messages = ArrayList<Message<*>>().observable()
    val connectedProperty = SimpleBooleanProperty(false)
    private var connected by connectedProperty

    private val user = User(username)
    private val server: ChatServer = TODO()

    fun sendText(message: String) {
        server.text(message)
    }

    /** Receives messages */
    private inner class Client : ChatClient {
        override fun text(user: User, message: String) {
            received(Message(user, message))
        }

        override fun binary(user: User, binary: Binary) {
            received(Message(user, binary))
        }

        override fun updateStatus(user: User, status: User.Status) {
            received(Message(user, status))
        }

        /** Close application on disconnect */
        override fun disconnect(reason: String?) {
            connected = false
        }

        private fun received(message: Message<*>) {
            Platform.runLater {
                messages.add(message)
            }
        }
    }
}

class Message<T : Any>(
    val user: User,
    val content: T,
    val time: LocalTime = LocalTime.now()
)