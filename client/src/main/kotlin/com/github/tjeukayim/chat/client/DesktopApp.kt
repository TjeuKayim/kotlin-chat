package com.github.tjeukayim.chat.client

import com.github.tjeukayim.chat.client.views.LoginView
import com.github.tjeukayim.chat.client.views.Styles
import javafx.application.Application
import tornadofx.App

/** TornadoFX application */
class DesktopApp : App(LoginView::class, Styles::class)

/**
 * The main method is needed to support the mvn jfx:run goal.
 */
fun main(args: Array<String>) {
    Application.launch(DesktopApp::class.java, *args)
}
