package com.github.tjeukayim.chat.client.views

import javafx.beans.property.SimpleStringProperty
import tornadofx.*

/** Let user choose a username */
class LoginView : View() {
    private val model: LoginModel by inject()

    override val root = form {
        fieldset("Choose your username") {
            textfield(model.username).required()
            button("Submit") {
                enableWhen(model.valid)
                action {
                    model.commit {
                        replaceWith(MainView::class)
                    }
                }
            }
        }
    }
}

class LoginModel : ViewModel() {
    val username = SimpleStringProperty()
}