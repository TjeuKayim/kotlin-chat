package com.github.tjeukayim.chat

interface ChatClient {
    fun text(user: User, message: String)
    fun binary(user: User, binary: Binary)
    fun updateStatus(user: User, status: User.Status)
    fun disconnect(reason: String? = null)
}

interface ChatServer {
    fun text(message: String)
    fun binary(binary: Binary)
}

data class User(val name: String) {
    enum class Status { ONLINE, OFFLINE }
}

data class Binary(val name: String, val bytes: ByteArray)