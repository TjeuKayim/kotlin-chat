package com.github.tjeukayim.chat.encoding

data class WebSocketMessage(
    val type: String,
    val arguments: List<Any>
)