package com.github.tjeukayim.chat

import com.github.tjeukayim.chat.encoding.MessageDecoder
import com.github.tjeukayim.chat.encoding.MessageEncoder
import io.mockk.mockk
import io.mockk.verify
import mu.KotlinLogging
import org.junit.jupiter.api.Test

private val logger = KotlinLogging.logger {}

class ChatMessagesTest {
    @Test
    fun `Encode and decode some method call`() {
        val (mock, encoder) = getMock<ChatServer>()
        encoder.text("Hello World")
        verify {
            mock.text("Hello World")
        }
    }
}

private inline fun <reified T : Any> getMock(): EncoderMock<T> {
    val mock: T = mockk(relaxed = true)
    val decode: MessageDecoder<T> = MessageDecoder(mock)
    val encoder: T = MessageEncoder.create {
        logger.info { String(it) }
        decode(it)
    }
    return EncoderMock(mock, encoder)
}

private data class EncoderMock<T>(
        val mock: T,
        val encoder: T
)